dojo.provide("controller.HomeController");

dojo.require("dijit._Widget");
dojo.require("dijit._Templated");
dojo.require("dijit.form.Form");
dojo.require("dijit.form.Button");
dojo.require("common.TextFormElement");
dojo.require("common.TextAreaFormElement");
dojo.require("common.CheckBoxElement");
dojo.require("common.MultiCheckElement");
dojo.require("common.RadioButtonsElement");

dojo.declare("controller.HomeController", [dijit._Widget, dijit._Templated], {
	widgetsInTemplate: true,
	templateString:
		dojo.cache("controller", "templates/HomeController.html"),
	baseClass: "HomeController",
	formNode: null,
	form: null,
	
	_addTextBox: function(name, label, attachPoint, description, promptMessage, required, regExp){
		var textbox = new common.TextFormElement({
			required: required,
			regExp: regExp,
			label: label,
			name: name,
			description: description,
			promptMessage: promptMessage
		});
	    dojo.place(textbox.domNode, attachPoint);
	},
	
	buildRendering: function(){
		this.inherited(arguments);
		this._createForm();
	},
	
	_createForm: function(){
		this.form = new dijit.form.Form({
	        encType: 'multipart/form-data',
	        action: '',
	        method: '',
	        onSubmit: dojo.hitch(this, "_onSubmit")
	    }, this.formNode);
	},
	
	_onSubmit: function(e){
		e.preventDefault();
		dojo.xhrPost({
    		handleAs: 'json',
    		url: '/giftjabber/api/giftSubmit',
    		load: dojo.hitch(this, "_formSaved"),
    		error: dojo.hitch(this, "_formError"),
    	});
	},
	
	_formSaved: function(){
		
	},
	
	_formError: function(){
		
	},
	
	postCreate: function(){
		this.inherited(arguments);
	}
	
});