dojo.provide("common.MultiCheckElement");

dojo.require("dijit._Widget");
dojo.require("dijit._Templated");
dojo.require("common.CheckBoxElement");

dojo.declare("common.MultiCheckElement", [dijit._Widget, dijit._Templated], {
	label:'',
	name:'',
	checkElements:[],
	widgetsInTemplate: false,
	templateString:
		dojo.cache("common", "templates/MultiCheckElement.html"),
	baseClass: "MultiBoxElement",
	buildRendering: function(){
		this.inherited(arguments);
		for(var i = 0; i < this.checkElements.length; i++){
	    	var node = this.rightCheckColumnNode;
	    	if(i % 2 == 0){
	    		node = this.leftCheckColumnNode;
	    	}
	    	var checkBox = new common.CheckBoxElement({
	            name: this.name,
	            label: this.checkElements[i],
	            value: this.checkElements[i],
	            checked: false
	        });
		    dojo.place(checkBox.domNode, node);
	    }
	}

});