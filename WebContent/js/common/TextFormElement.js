dojo.provide("common.TextFormElement");

dojo.require("dijit._Widget");
dojo.require("dijit._Templated");
dojo.require("dijit.form.ValidationTextBox");

dojo.declare("common.TextFormElement", [dijit._Widget, dijit._Templated], {
	required:false,
	name:'',
	label:'',
	description:'',
	promptMessage:'',
	regExp:null,
	widgetsInTemplate: true,
	templateString:
		dojo.cache("common", "templates/TextFormElement.html"),
	baseClass: "TextFormElement",
	buildRendering: function(){
		this.inherited(arguments);
		this.labelNode.innerHTML = this.label;
		if(this.required != false){
			this.labelNode.innerHTML += "<em>*<em>";
		}
		if(typeof this.regExp === "string"){
			dojo.attr(this.textBoxNode, "regExp", this.regExp);
		}
		this.labelNode.innerHTML += ":";
		if(this.description != ''){
			dojo.place("<p1>" + this.description + "</p1>", this.labelNode, "after");
		}
	}
});