dojo.provide("common.TextAreaFormElement");

dojo.require("dijit._Widget");
dojo.require("dijit._Templated");
dojo.require("dijit.form.Textarea");

dojo.declare("common.TextAreaFormElement", [dijit._Widget, dijit._Templated], {
	name:'',
	label:'',
	placeHolder:'',
	widgetsInTemplate: true,
	templateString:
		dojo.cache("common", "templates/TextAreaFormElement.html"),
	baseClass: "TextAreaFormElement"
});