dojo.provide("common.RadioButtonsElement");

dojo.require("dijit._Widget");
dojo.require("dijit._Templated");
dojo.require("common.RadioButtonElement");

dojo.declare("common.RadioButtonsElement", [dijit._Widget, dijit._Templated], {
	name:'',
	label:'',
	values:[],
	selected:'',
	templateString:
		dojo.cache("common", "templates/RadioButtonsElement.html"),
	baseClass: "RadioButtonsElement",
	buildRendering: function(){
		this.inherited(arguments);
		for(var i = 0; i < this.values.length; i++){
			var value = this.values[i];
			var button = new common.RadioButtonElement({
				checked: (this.selected == value),
				label: value,
				name: this.name,
				value: value
			});
			dojo.place(button.domNode, this.labelNode, 'after');
		}
	}
});