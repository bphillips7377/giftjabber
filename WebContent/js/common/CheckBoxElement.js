dojo.provide("common.CheckBoxElement");

dojo.require("dijit._Widget");
dojo.require("dijit._Templated");
dojo.require("dijit.form.CheckBox");

dojo.declare("common.CheckBoxElement", [dijit._Widget, dijit._Templated], {
	checked:false,
	name:'',
	label:'',
	value:'',
	widgetsInTemplate: true,
	templateString:
		dojo.cache("common", "templates/CheckBoxElement.html"),
	baseClass: "CheckBoxElement",
	buildRendering: function(){
		this.inherited(arguments);
		this.labelNode.innerHTML = this.label;
	}
});