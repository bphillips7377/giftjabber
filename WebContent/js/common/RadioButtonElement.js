dojo.provide("common.RadioButtonElement");

dojo.require("dijit._Widget");
dojo.require("dijit._Templated");
dojo.require("dijit.form.CheckBox");

dojo.declare("common.RadioButtonElement", [dijit._Widget, dijit._Templated], {
	checked:false,
	name:'',
	label:'',
	value:'',
	widgetsInTemplate: true,
	templateString:
		dojo.cache("common", "templates/RadioButtonElement.html"),
	baseClass: "RadioButtonElement",
	buildRendering: function(){
		this.inherited(arguments);
		this.labelNode.innerHTML = this.label;
	}
});