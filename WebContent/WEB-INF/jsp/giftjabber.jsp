<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html>
<head>
    <title>Gift Registry</title>
	<link rel="shortcut icon" href="images/favicon.gif" type="image/x-icon"> 
    <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
    <link href="css/main.css" media="screen" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/dojo/1.6/dijit/themes/claro/claro.css" />
    <script>
		// We're specifying our Dojo Configuration this way,
		// as it's a bit more complex and easier to type out
		// than a data-dojo-config string
		var dojoConfig = {				
				isDebug: true,				
				// The next two pieces define how we can load our custom modules				
				// baseUrl species where to work from as a baseline				
				// Here we're setting it to be the current directory				
				baseUrl: './',				
				// modulePaths takes an object with keys that are the namespace,
				// and values that are the path - in this case, just a folder
				// named 'custom' under the baseUrl	
				modulePaths: {
					'common': 'js/common',
					'controller': 'js/controller'
				}
		}
    </script>
    <script src="http://ajax.googleapis.com/ajax/libs/dojo/1.6.0/dojo/dojo.xd.js"></script>
</head>
<body class="claro">
	<script type="text/javascript">
		dojo.require("controller.HomeController");
		dojo.addOnLoad(function(){
			try{
				var homeWidget = new controller.HomeController({}, dojo.byId('homeController'));
				homeWidget.startup();
			}catch (e){
				console.error(e);
			}
		});
	</script>
	<div class="content" id="homeController"></div>
</body></html>