/*
 * Copyright 2010-2011 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.giftjabber.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.giftjabber.entity.Gift;
import com.giftjabber.util.StageUtils;
import com.spaceprogram.simplejpa.EntityManagerFactoryImpl;

public class GiftjabberDAOSimpleDBImpl implements GiftjabberDAO {

    private static Map<String, String> properties = new HashMap<String, String>();

    private static EntityManagerFactoryImpl factory = new EntityManagerFactoryImpl("TravelLog" + StageUtils.getResourceSuffixForCurrentStage(), properties);

    @SuppressWarnings("unchecked")
	@Override
	public List<Gift> getGiftIdeas() {
		EntityManager em = null;

        try {
            em = factory.createEntityManager();
            Query query = em.createQuery("select g from com.amazon.aws.samplecode.travellog.entity.Gift g");
            return (List<Gift>)query.getResultList();
        }
        finally {
            if (em!=null) {
                em.close();
            }
        }
	}
    
    public void saveGiftIdea (Gift gift) {
        EntityManager em = null;
        //Storage fails if id is an empty string, so nullify it
        if (gift.getId()!=null && gift.getId().equals("")) {
        	gift.setId(null);
        }
        try {
            em = factory.createEntityManager();
            em.persist(gift);

        }
        finally {
            if (em!=null) {
                em.close();
            }
        }
    }
}
