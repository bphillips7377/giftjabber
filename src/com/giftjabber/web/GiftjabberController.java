/*
 * Copyright 2010-2011 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
package com.giftjabber.web;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.giftjabber.dao.GiftjabberDAO;

/**
 * This is the core of the TravelLog functionality.  It's a Spring controller implemented
 * using annotations.  Most methods for loading and storing journals, entries, comments and photos
 * are initiated in this class.
 */
@Controller
public class GiftjabberController {

    private GiftjabberDAO dao;
    private static final Logger logger=Logger.getLogger(GiftjabberController.class.getName());


    /**
     * AWS Elastic Beanstalk checks your application's health by periodically
     * sending an HTTP HEAD request to a resource in your application. By
     * default, this is the root or default resource in your application,
     * but can be configured for each environment.
     *
     * Here, we report success as long as the app server is up, but skip
     * generating the whole page since this is a HEAD request only. You
     * can employ more sophisticated health checks in your application.
     *
     * @param model the spring model for the request
     */
    @RequestMapping(value="/home", method=RequestMethod.HEAD)
    public void doHealthCheck(HttpServletResponse response) {
        response.setContentLength(0);
        response.setStatus(HttpServletResponse.SC_OK);
    }

    /**
     * The main request handler that builds out the home page for the journal
     * @param model the spring model for the request
     */
    @RequestMapping(value="/giftjabber", method=RequestMethod.GET)
    public void doHome (ModelMap model) {
    	logger.log(Level.ALL, "starting home page");
    }
    
    @RequestMapping(value="/api/giftSubmit")
    public ModelAndView giftSubmit (ModelMap model) {
    	System.out.println("giftSubmitted!");
    	logger.log(Level.ALL, "starting home page");
    	return new ModelAndView("/giftjabber");
    }


    @Autowired
    public void setTravelLogDAO (GiftjabberDAO dao) {
        this.dao = dao;
    }

    /**
     * Method establishes the transformation of incoming date strings into Date objects
     * @param binder the spring databinder object that we connect to the date editor
     */
    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
        dateFormat.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }


}
